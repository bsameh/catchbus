//
//  MovingAnnotationView.h
//  CatchBus
//
//  Created by Bassem Sameh on 11/23/15.
//  Copyright © 2015 Catch Bus. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MovingAnnotationView : MKAnnotationView

@end
