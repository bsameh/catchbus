//
//  BusPosition.h
//  CatchBus
//
//  Created by Bassem Sameh on 11/23/15.
//  Copyright © 2015 Catch Bus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BusPosition : NSObject

+ (void)lastKnownBusLocationWithSuccessBlock:(APISuccessBlock)successBlock failureBlock:(APIFailureBlock)failureBlock;

@end
