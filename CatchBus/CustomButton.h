//
//  CustomButton.h
//  CatchBus
//
//  Created by Bassem Sameh on 11/19/15.
//  Copyright © 2015 Catch Bus. All rights reserved.
//

IB_DESIGNABLE

#import <UIKit/UIKit.h>

@interface CustomButton : UIButton

@property (nonatomic) IBInspectable UIColor *customTextColor;
@property (nonatomic) IBInspectable UIColor *customBackgroundColor;
@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable UIColor *shadowColor;

@property (nonatomic) IBInspectable CGFloat opacity;
@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic) IBInspectable CGFloat cornerRadius;
@property (nonatomic) IBInspectable CGFloat shadowOpacity;

@end
