//
//  LoginService.h
//  CatchBus
//
//  Created by Bassem Sameh on 11/19/15.
//  Copyright © 2015 Catch Bus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginService : NSObject

+ (void)loginUserWithUsername:(NSString *)username
                     password:(NSString *)password
                 successBlock:(APISuccessBlock)successBlock
                 failureBlock:(APIFailureBlock)failureBlock;

@end
