//
//  LoginViewController.m
//  CatchBus
//
//  Created by Bassem Sameh on 11/19/15.
//  Copyright © 2015 Catch Bus. All rights reserved.
//

#import "LoginViewController.h"
#import "MainViewController.h"
#import "LoginService.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.usernameTextField.text = @"doo2a_9@hotmail.com";
    self.passwordTextField.text = @"pass@word1";
}

- (IBAction)loginUser:(id)sender
{
    NSString *username = self.usernameTextField.text;
    NSString *password = self.passwordTextField.text;
    
    [LoginService loginUserWithUsername:username
                               password:password
                           successBlock:^(id data) {
                               
//                               UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                               MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"mainViewController"];
                               NSLog(@"Dismiss login");
                               [self dismissViewControllerAnimated:YES completion:nil];
                           } failureBlock:^(NSError *error) {
                               
                           }];
}

@end
