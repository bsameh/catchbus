//
//  SingleBorderTextField.m
//  CatchBus
//
//  Created by Bassem Sameh on 11/19/15.
//  Copyright © 2015 Catch Bus. All rights reserved.
//

#import "SingleBorderTextField.h"

static CGFloat const kDefaultTextOpacity = 0.6f;
static CGFloat const kDefaultPlaceholderOpacity = 0.6f;
static CGFloat const kDefaultBorderOpacity = 0.6f;

@implementation SingleBorderTextField

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)prepareForInterfaceBuilder
{
    [self setup];
}

- (void)setup
{
    [self addBottomBorder];
    
    UIColor *textColor = self.customTextColor ? self.customTextColor : [UIColor colorWithWhite:255.0f alpha:kDefaultTextOpacity];
    UIColor *placeholderColor = self.customPlaceholderColor ? self.customPlaceholderColor : [UIColor colorWithWhite:255.0f alpha:kDefaultPlaceholderOpacity];
    
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:self.placeholder attributes:@{ NSForegroundColorAttributeName : placeholderColor}];
    self.attributedPlaceholder = attributedString;
    
    self.textColor = textColor;
}

- (void)addBottomBorder
{
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, self.frame.size.height - 1, self.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = self.borderColor? self.borderColor.CGColor : [UIColor whiteColor].CGColor;
    bottomBorder.opacity = self.borderOpacity ? self.borderOpacity : kDefaultBorderOpacity;
    
    [self.layer addSublayer:bottomBorder];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
