//
//  MovingAnnotation.h
//  CatchBus
//
//  Created by Bassem Sameh on 11/23/15.
//  Copyright © 2015 Catch Bus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MovingAnnotation : NSObject <MKAnnotation>

// Make coordinate settable.
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate;

@end
