//
//  MeetingPoint.m
//  CatchBus
//
//  Created by Bassem Sameh on 11/19/15.
//  Copyright © 2015 Catch Bus. All rights reserved.
//

#import "MeetingPoint.h"

static NSString * const kGetMeetingPointsPath = @"Student/MeetingPoint";
static NSString * const kCreatMeetingPointPath = @"Student/SendMeetingPoint";

@implementation MeetingPoint

+ (void)fetchMeetingPointsWithSuccessBlock:(APISuccessBlock)successBlock failureBlock:(APIFailureBlock)failureBlock
{
    NSDictionary *parameters = @{@"userName" : @"doo2a_9@hotmail.com"};
    
    [[APIClient sharedClient] startGetRequestWithPath:kGetMeetingPointsPath
                                           parameters:parameters
                                         successBlock:^(id data) {
                                             successBlock(data);
                                         } failureBlock:^(NSError *error) {
                                             failureBlock(error);
    }];
}

+ (void)addMeetingPointWithCoordinate:(CLLocationCoordinate2D)coordinate threshold:(NSNumber *)threshold isPrimary:(BOOL)isPrimary successBlock:(APISuccessBlock)successBlock failureBlock:(APIFailureBlock)failureBlock
{
    NSDictionary *parameters = @{@"userName" : @"doo2a_9@hotmail.com",
                                 @"name" : @"test",
                                 @"id" : @"",
                                 @"lat" : @30.086628,
                                 @"lng" : @31.334325,
                                 @"distance" : @1,
                                 @"isPrimary" : @"true"};

    [[APIClient sharedClient] startPostRequestWithPath:kCreatMeetingPointPath
                                            parameters:parameters
                                          successBlock:^(id data) {
                                              successBlock(data);
                                        } failureBlock:^(NSError *error) {
                                            failureBlock(error);
    }];
}

+ (NSDictionary *)nearestMeetingPointFromPoints:(NSArray *)meetingPoints
{
    NSDictionary *nearestPoint = [NSDictionary new];
    
    for(NSDictionary *point in meetingPoints) {
        float distance = [point[@"Distance"] floatValue];
        float nearestDistance = [nearestPoint[@"Distance"] floatValue];
        
        if(distance < nearestDistance || nearestDistance == 0) {
            nearestPoint = point;
        }
    }
    
    return nearestPoint;
}

+ (CLLocationCoordinate2D)coordinateFromMeetingPoint:(NSDictionary *)meetingPoint
{
    float latitude = [meetingPoint[@"Lat"] floatValue];
    float longtitude = [meetingPoint[@"Lng"] floatValue];
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longtitude);
    
    return coordinate;
}

+ (void)addressForCoordinate:(CLLocationCoordinate2D)pinCoordinate withCompletionBlock:(void (^)(NSString *formattedAddress))completionBlock
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    CLLocation *location = [[CLLocation alloc] initWithLatitude:pinCoordinate.latitude longitude:pinCoordinate.longitude];
    
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        
        NSString *fallbackString = @"Street name not found";
        
        if(!error) {
            CLPlacemark *closestPlacemark = [placemarks firstObject];
            NSString *placemarkFormattedAddress = closestPlacemark.thoroughfare;
            placemarkFormattedAddress = placemarkFormattedAddress.length > 0 ? placemarkFormattedAddress : fallbackString;
            
            completionBlock(placemarkFormattedAddress);
        }
        else {
            completionBlock(fallbackString);
        }
    }];
}

@end
