//
//  BaseViewController.h
//  CatchBus
//
//  Created by Bassem Sameh on 11/19/15.
//  Copyright © 2015 Catch Bus. All rights reserved.
//

// Frameworks
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

// Custom Frameworks
#import <pop/POP.h>

// Custom views
#import "DGActivityIndicatorView.h"
#import "CustomButton.h"

@interface BaseViewController : UIViewController

- (void)showActivityIndicator;
- (void)hideActivityIndicator;

@end
