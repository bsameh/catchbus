//
//  MovingAnnotation.m
//  CatchBus
//
//  Created by Bassem Sameh on 11/23/15.
//  Copyright © 2015 Catch Bus. All rights reserved.
//

#import "MovingAnnotation.h"

@implementation MovingAnnotation

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate
{
    if(self = [super init]) {
        self.coordinate = coordinate;
    }
    
    return self;
}

@end
