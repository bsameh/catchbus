//
//  MeetingPoint.h
//  CatchBus
//
//  Created by Bassem Sameh on 11/19/15.
//  Copyright © 2015 Catch Bus. All rights reserved.
//

#import <Foundation/Foundation.h>

@import CoreLocation;

@interface MeetingPoint : NSObject

+ (void)fetchMeetingPointsWithSuccessBlock:(APISuccessBlock)successBlock failureBlock:(APIFailureBlock)failureBlock;
+ (void)addMeetingPointWithCoordinate:(CLLocationCoordinate2D)coordinate threshold:(NSNumber *)threshold isPrimary:(BOOL)isPrimary successBlock:(APISuccessBlock)successBlock failureBlock:(APIFailureBlock)failureBlock;

+ (NSDictionary *)nearestMeetingPointFromPoints:(NSArray *)meetingPoints;
+ (CLLocationCoordinate2D)coordinateFromMeetingPoint:(NSDictionary *)meetingPoint;
+ (void)addressForCoordinate:(CLLocationCoordinate2D)pinCoordinate withCompletionBlock:(void (^)(NSString *formattedAddress))completionBlock;

@end
