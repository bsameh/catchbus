//
//  BusPosition.m
//  CatchBus
//
//  Created by Bassem Sameh on 11/23/15.
//  Copyright © 2015 Catch Bus. All rights reserved.
//

#import "BusPosition.h"

static NSString * const kGetLastKnownBusLocation = @"Client/BusPosition";

@implementation BusPosition

+ (void)lastKnownBusLocationWithSuccessBlock:(APISuccessBlock)successBlock failureBlock:(APIFailureBlock)failureBlock
{
    NSDictionary *parameters = @{@"userName" : @"doo2a_9@hotmail.com"};
    
    [[APIClient sharedClient] startGetRequestWithPath:kGetLastKnownBusLocation
                                           parameters:parameters
                                         successBlock:^(id data) {
                                             successBlock(data);
                                         } failureBlock:^(NSError *error) {
                                             failureBlock(error);
                                         }];
}

@end
