//
//  BaseViewController.m
//  CatchBus
//
//  Created by Bassem Sameh on 11/19/15.
//  Copyright © 2015 Catch Bus. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController
{
    DGActivityIndicatorView *activityIndicatorView;
    UILabel *loadingLabel;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)showActivityIndicator
{
    if(activityIndicatorView) {
        return;
    }
    
    UIColor *activityIndicatorTintColor = [UIColor colorWithRed:0.0f
                                                          green:40.0f/255.0f
                                                           blue:230.0f/255.0f
                                                          alpha:0.7f];
    
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeTriplePulse tintColor:activityIndicatorTintColor size:50.0f];
    activityIndicatorView.frame = self.view.frame;
    activityIndicatorView.backgroundColor = [UIColor colorWithWhite:255.0f alpha:0.8f];
    
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    
    loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - 40.0f, 35.0f)];
    loadingLabel.center = CGPointMake(activityIndicatorView.center.x, activityIndicatorView.center.y + 40.0f);
    loadingLabel.text = @"Fetching meeting points";
    loadingLabel.font = [UIFont systemFontOfSize:13.0f weight:UIFontWeightLight];
    loadingLabel.textColor = [UIColor colorWithWhite:0.0f alpha:0.3f];
    loadingLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.view addSubview:loadingLabel];
}

- (void)hideActivityIndicator
{
    [UIView animateWithDuration:0.8f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        activityIndicatorView.alpha = 0.0f;
        loadingLabel.alpha = 0.0f;
    } completion:^(BOOL finished) {
        if(finished) {
            [activityIndicatorView stopAnimating];
            [activityIndicatorView removeFromSuperview];
            [loadingLabel removeFromSuperview];
        }
    }];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

@end
