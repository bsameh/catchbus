//
//  SingleBorderTextField.h
//  CatchBus
//
//  Created by Bassem Sameh on 11/19/15.
//  Copyright © 2015 Catch Bus. All rights reserved.
//

IB_DESIGNABLE

#import <UIKit/UIKit.h>

@interface SingleBorderTextField : UITextField

@property (nonatomic) IBInspectable UIColor *customTextColor;
@property (nonatomic) IBInspectable UIColor *customPlaceholderColor;
@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable CGFloat borderOpacity;

@end
