//
//  LoginService.m
//  CatchBus
//
//  Created by Bassem Sameh on 11/19/15.
//  Copyright © 2015 Catch Bus. All rights reserved.
//

#import "LoginService.h"

@implementation LoginService

+ (void)loginUserWithUsername:(NSString *)username password:(NSString *)password successBlock:(APISuccessBlock)successBlock failureBlock:(APIFailureBlock)failureBlock
{
    NSDictionary *paramters = @{@"userName" : username,
                                @"password" : password};
    
    [[APIClient sharedClient] startGetRequestWithPath:@"Client/ValidateClinet" parameters:paramters successBlock:^(id data) {
        successBlock(data);
    } failureBlock:^(NSError *error) {
        failureBlock(error);
    }];
}

@end
