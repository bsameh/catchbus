//
//  CustomButton.m
//  CatchBus
//
//  Created by Bassem Sameh on 11/19/15.
//  Copyright © 2015 Catch Bus. All rights reserved.
//

#import "CustomButton.h"

@implementation CustomButton

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)prepareForInterfaceBuilder
{
    [self setup];
}

- (void)setup
{
    self.backgroundColor = self.customBackgroundColor;
    self.alpha = self.opacity ? self.opacity : 1.0f;
    
    self.titleLabel.textColor = self.customTextColor ? self.customTextColor : [UIColor whiteColor];
    
    self.layer.cornerRadius = self.cornerRadius ? self.cornerRadius : 0.0f;
    self.layer.borderWidth = self.borderWidth ? self.borderWidth : 0.0f;
    self.layer.borderColor = self.borderColor ? self.borderColor.CGColor : self.backgroundColor.CGColor;
}

@end
