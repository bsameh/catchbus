//
//  MainViewController.m
//  CatchBus
//
//  Created by Bassem Sameh on 11/19/15.
//  Copyright © 2015 Catch Bus. All rights reserved.
//

#import "MainViewController.h"

// Services
#import "MeetingPoint.h"
#import "BusPosition.h"

//
#import "MovingAnnotation.h"

@implementation MainViewController
{
    CLLocationManager *locationManager;
    
    NSMutableArray *meetingPoints;
    NSDictionary *selectedMeetingPoint;
    
    UIVisualEffectView *mapVisualEffectView;
    
    float fakeBusLatitude;
    float fakeBusLongitutde;
}

#pragma mark - View controller lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;
    
    UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchGesture:)];
    pinchRecognizer.delegate = self;
    //self.view
    [self.mapView addGestureRecognizer:pinchRecognizer];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self setupLocationManager];
    [self setupMapView];
    
    fakeBusLatitude = 30.082376;
    fakeBusLongitutde = 31.337855;
 
    [self fetchMeetingPoints];
    
    [NSTimer scheduledTimerWithTimeInterval:3.0f
                                     target:self
                                   selector:@selector(fetchBusPosition)
                                   userInfo:nil
                                    repeats:YES];
}

#pragma mark - Setup

- (void)setupLocationManager
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    [locationManager requestWhenInUseAuthorization];
    [locationManager startUpdatingLocation];
}

- (void)setupMapView
{
    self.mapView.showsUserLocation = YES;
    self.mapView.userTrackingMode = MKUserTrackingModeFollow;
    [self.mapView setCenterCoordinate:self.mapView.userLocation.coordinate animated:YES];
}

#pragma mark - Appearance

- (void)blurMapView
{
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    
    mapVisualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    mapVisualEffectView.frame = self.view.frame;
    //mapVisualEffectView.alpha = 0.8f;
    
    //[self.mapView addSubview:mapVisualEffectView];
}

- (void)removeBlurFromMapView
{
    [mapVisualEffectView removeFromSuperview];
}

- (void)handlePinchGesture:(UIPinchGestureRecognizer *)pinchRecognizer
{
    for(id <MKAnnotation> annotation in self.mapView.annotations) {
        if([annotation isKindOfClass:[MovingAnnotation class]]) {
            double zoomLevel = [self.mapView region].span.latitudeDelta;
            //double scale = -1 * sqrt((double)(1 - pow((zoomLevel/100.0), 2.0))) + 1.1; // This is a circular scale function where at zoom level 0 scale is 0.1 and at zoom level 20 scale is 1.1
            double scale = zoomLevel / 50.0f;
            // Option #1
            MKAnnotationView *annotationView = [self.mapView viewForAnnotation:annotation];
            //annotationView.transform = CGAffineTransformMakeScale(scale, scale);
            
//            // Option #2
//            UIImage *pinImage = [UIImage imageNamed:@"YOUR_IMAGE_NAME_HERE"];
//            pinView.image = [pinImage resizedImage:CGSizeMake(pinImage.size.width * scale, pinImage.size.height * scale) interpolationQuality:kCGInterpolationHigh];
        }
    }
}

#pragma mark - Logic

- (void)fetchMeetingPoints
{
    //[self showActivityIndicator];
    [self blurMapView];
    
    [MeetingPoint fetchMeetingPointsWithSuccessBlock:^(id data) {
        
        NSLog(@"Meeting points: %@", data);
        
        meetingPoints = data;
        selectedMeetingPoint = [MeetingPoint nearestMeetingPointFromPoints:meetingPoints];
        
        [self showMeetingPointsAsPins];
        [self removeBlurFromMapView];
        [self hideActivityIndicator];
        
    } failureBlock:^(NSError *error) {
        [self hideActivityIndicator];
    }];
}

- (void)fetchBusPosition
{
    [BusPosition lastKnownBusLocationWithSuccessBlock:^(id data) {
        
        NSDictionary *busPosition = data;
//        float latitude = [busPosition[@"lat"] floatValue];
//        float longitude = [busPosition[@"lng"] floatValue];
        
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(fakeBusLatitude, fakeBusLongitutde);
        
#warning SIMULATION
        fakeBusLatitude += 0.00005;
        //fakeBusLatitude += 0.000005;
        
        [self updateBusLocationWithCoordinate:coordinate];
    } failureBlock:^(NSError *error) {
        
    }];
}

- (void)createMeetingPoint
{
    CLLocationCoordinate2D coordinate = [MeetingPoint coordinateFromMeetingPoint:selectedMeetingPoint];
    
    [MeetingPoint addMeetingPointWithCoordinate:coordinate threshold:@100 isPrimary:YES successBlock:^(id data) {
        NSLog(@"Successfully added custom meeting point.");
    } failureBlock:^(NSError *error) {
        NSLog(@"Failed to add custom meeting point.");
    }];

}

- (void)showMeetingPointsAsPins
{
    for(NSDictionary *meetingPoint in meetingPoints) {
        CLLocationCoordinate2D coordinate = [MeetingPoint coordinateFromMeetingPoint:meetingPoint];
        //NSInteger meetingPointName = [meetingPoint[@"Name"] integerValue];
        
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        annotation.coordinate = coordinate;
        //annotation.title = [NSString stringWithFormat:@"%@", meetingPointName];
        
        [self.mapView addAnnotation:annotation];
    }
}

- (void)updateBusLocationWithCoordinate:(CLLocationCoordinate2D)coordinate
{
    MovingAnnotation *busAnnotation;
    
    for(id<MKAnnotation> annotation in self.mapView.annotations) {
        if([annotation isKindOfClass:[MovingAnnotation class]]) {
            busAnnotation = (MovingAnnotation *)annotation;
            break;
        }
    }
    
    
    if(!busAnnotation) {
        busAnnotation = [[MovingAnnotation alloc] initWithCoordinate:coordinate];
        [self.mapView addAnnotation:busAnnotation];
    }
    else {
        busAnnotation.coordinate = coordinate;
    }
}

- (void)panToCoordinate:(CLLocationCoordinate2D)coordinate
{
    MKCoordinateRegion region = MKCoordinateRegionMake(coordinate, MKCoordinateSpanMake(0.015f, 0.015f));
    [self.mapView setRegion:region animated:YES];
}

#pragma mark - MKMapView

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    CLLocationCoordinate2D selectedCoordinate = view.annotation.coordinate;
    
    [self panToCoordinate:selectedCoordinate];
    [self updateMeetingPointAddressWithCoordinate:selectedCoordinate];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    else if([annotation isKindOfClass:[MovingAnnotation class]]) {
        MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                                        reuseIdentifier:@"bus"];
        annotationView.image = [UIImage imageNamed:@"bus-annotation"];
        
        return annotationView;
    }
    else {
        UIColor *annotationTintColor = [UIColor colorWithRed:90.0f/255.0f
                                                       green:240.0f/255.0f
                                                        blue:140.0f/255.0f
                                                       alpha:1.0f];
        
        MKPinAnnotationView *pinAnnotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
                                                                                 reuseIdentifier:@"pin"];
        pinAnnotationView.pinTintColor = annotationTintColor;
        pinAnnotationView.animatesDrop = YES;
        
        return pinAnnotationView;
    }
    
    return nil;
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
//    for(id <MKAnnotation> annotation in self.mapView.annotations) {
//        if([annotation isKindOfClass:[MovingAnnotation class]]) {
//            double zoomLevel = [self.mapView region].span.latitudeDelta;
//            double scale = -1 * sqrt((double)(1 - pow((zoomLevel/20.0), 2.0))) + 1.1; // This is a circular scale function where at zoom level 0 scale is 0.1 and at zoom level 20 scale is 1.1
//            
//            // Option #1
//            MKAnnotationView *annotationView = [self.mapView viewForAnnotation:annotation];
//            annotationView.transform = CGAffineTransformMakeScale(scale, scale);
//            
//            //            // Option #2
//            //            UIImage *pinImage = [UIImage imageNamed:@"YOUR_IMAGE_NAME_HERE"];
//            //            pinView.image = [pinImage resizedImage:CGSizeMake(pinImage.size.width * scale, pinImage.size.height * scale) interpolationQuality:kCGInterpolationHigh];
//        }
//    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    //CLLocation *location = [locations lastObject];
    //[self panToCoordinate:location.coordinate];
}

- (void)updateMeetingPointAddressWithCoordinate:(CLLocationCoordinate2D)coordinate
{
    [MeetingPoint addressForCoordinate:coordinate
                           withCompletionBlock:^(NSString *formattedAddress) {
                               [self.meetingPointButton setTitle:formattedAddress forState:UIControlStateNormal];
                           }];
}

#pragma mark - IBActions

- (IBAction)panToNearestMeetingPoint:(id)sender
{
    NSDictionary *nearestMeetingPoint = [MeetingPoint nearestMeetingPointFromPoints:meetingPoints];
    
    CLLocationCoordinate2D nearestCoordinate = [MeetingPoint coordinateFromMeetingPoint:nearestMeetingPoint];
    
    [self panToCoordinate:nearestCoordinate];
    [self updateMeetingPointAddressWithCoordinate:nearestCoordinate];
}

- (IBAction)addCustomMeetingPoint:(id)sender
{
    [self createMeetingPoint];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(nonnull UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

@end
