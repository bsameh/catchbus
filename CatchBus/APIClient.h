//
//  APIClient.h
//  CatchBus
//
//  Created by Bassem Sameh on 11/19/15.
//  Copyright © 2015 Catch Bus. All rights reserved.
//

#import "AFHTTPSessionManager.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "AFNetworkActivityLogger.h"

@interface APIClient : AFHTTPSessionManager

typedef void (^APISuccessBlock)(id data);
typedef void (^APIFailureBlock)(NSError *error);

+ (APIClient *)sharedClient;

- (void)startGetRequestWithPath:(NSString *)path
                     parameters:(NSDictionary *)parameters
                   successBlock:(APISuccessBlock)success
                   failureBlock:(APIFailureBlock)failure;

- (void)startPostRequestWithPath:(NSString *)path
                      parameters:(NSDictionary *)parameters
                    successBlock:(APISuccessBlock)success
                    failureBlock:(APIFailureBlock)failure;

@end
